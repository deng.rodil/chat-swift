//
//  LoginPresenter.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/10/22.
//

import Foundation
import SwiftUI

extension LoginView {
    class ViewModel: ObservableObject {

        @Dependency private var dataService: DataService
        
        @Published var isLoggedIn = false
        @Published var user: User?
        @Published var email = ""
        @Published var password = ""
        @Published var picture: UIImage?
        @Published var errorMessage = ""
        @Published private(set) var isBusy = false

        func createAccount(_ completion: @escaping () -> ()) {
            errorMessage = ""
            isBusy = true
            dataService.createAccount(email: email, password: password) { [weak self] result in
                switch result {
                case .success(let user):
                    print("User created: \(user.uid)")
                    self?.isBusy = false
                    self?.user = user
                    completion()
                case .failure(let error):
                    self?.isBusy = false
                    self?.errorMessage = error.localizedDescription
                }
            }
        }
        
        func login(_ completion: @escaping () -> ()) {
            errorMessage = ""
            isBusy = true
            dataService.signIn(email: email, password: password) { [weak self] result in
                switch result {
                case .success(let user):
                    print("User authenticated: \(user.uid)")
                    self?.isBusy = false
                    self?.user = user
                    completion()
                case .failure(let error):
                    self?.isBusy = false
                    self?.errorMessage = error.localizedDescription
                }
            }
        }
    }
}
