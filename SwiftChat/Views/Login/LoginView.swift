//
//  ContentView.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/3/22.
//

import SwiftUI
import Firebase

struct LoginView: View {

    @StateObject private var vm: LoginView.ViewModel = .init()
    var didLogin: (User) -> ()

    @State private var isLoginMode = true
    @State private var isAuthenticating = false
    
    var body: some View {

        ScrollView {
            VStack(spacing: 16) {
                // Tab
                Picker(selection: $isLoginMode) {
                    Text("Login")
                        .tag(true)
                    Text("Create Account")
                        .tag(false)
                } label: {
                    Text("Picker here")
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding()
                
                // Email / Password
                Group {
                    TextField("Email", text: $vm.email)
                        .keyboardType(.emailAddress)
                        .autocapitalization(.none)
                    
                    SecureField("Password", text: $vm.password)
                }
                .padding(12)
                .background(Color.white)
                
                // Action button
                Button {
                    handleMainAction()
                } label: {
                    HStack {
                        Spacer()
                        Text(actionButtonText)
                            .foregroundColor(.white)
                            .padding(.vertical, 10)
                            .font(.system(size: 14, weight: .semibold))
                        Spacer()
                    }.background(vm.isBusy ? Color.gray : Color.blue)
                }
                .disabled(vm.isBusy)
                .padding()
 
                // Status message
                Text(vm.errorMessage)
                    .foregroundColor(.red)
                
                // Test logins
//                testLogins
            }
            .background(Color(.init(white: 0, alpha: 0.05))
                            .ignoresSafeArea())
        }
        .navigationTitle(isLoginMode ? "Login" : "Create Account")
        .navigationBarBackButtonHidden(true)
    }
    
    private let testUsers = [User.fie, User.alisa]
    private var testLogins: some View {
        VStack {
            
            ForEach(testUsers) { user in
                Button {
                    vm.email = user.email
                    vm.password = "123456"
                    vm.login {
                        didLogin(user)
                    }
                } label: {
                    HStack {
                        Spacer()
                        Text("Log in as \(user.email)")
                            .foregroundColor(.white)
                            .padding(.vertical, 10)
                            .font(.system(size: 14, weight: .semibold))
                        Spacer()
                    }.background(vm.isBusy ? Color.gray : Color.blue)
                }
                .padding()
            }
        }
        
    }
    
    private func handleMainAction() {
        
        if isLoginMode {
            vm.login {
                didLogin(vm.user!)
            }
        }
        else {
            vm.createAccount {
                didLogin(vm.user!)
            }
        }
    }
    
    private var actionButtonText: String {
        if vm.isBusy {
            return "..."
        } else if isLoginMode {
            return "Login"
        } else {
            return "Create Account"
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            LoginView { user in
                
            }
        }
    }
}
