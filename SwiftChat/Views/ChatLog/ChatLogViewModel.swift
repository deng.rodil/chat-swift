//
//  ChatLogViewModel.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/17/22.
//

import Foundation

extension ChatLogView {
    class ViewModel: ObservableObject {
        
        @Dependency private var dataService: DataService
        
        private(set) var sender: User
        private(set) var channel: Channel?
        private var receiver: User?
        private var users: [User] = []
        private var channelId: String?
        
        @Published var messages = [Message]()
        @Published var chatText = ""
        @Published var chatRooomTitle = ""
        @Published var recentMessage: Message?
        @Published var recentMessageSent: Message?
        
        init(channelId: String, sender: User) {
            self.sender = sender
            self.channelId = channelId
        }
        
        // Used for direct channels
        init(receiver: User, sender: User) {
            self.sender = sender
            self.receiver = receiver
        }
        
        func fetchData() {
            if let uid = channelId {
                fetchChannel(uid: uid)
            }
            else {
                fetchDirectChannel()
            }
        }
        
        func sendText() {
            guard let channel = channel else { return }
            
            dataService.sendText(chatText, sender: sender, channel: channel) { [weak self] result in
                switch result {
                case .success(let message):
                    self?.recentMessageSent = message
                    print("Message sent: \(message.text)")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            self.chatText = ""
        }
        
        private func fetchDirectChannel() {
            guard let receiver = receiver else { return }
            
            dataService.getDirectChannel(from: sender, to: receiver) { [weak self] result in
                switch result {
                case .success(let channel):
                    self?.channel = channel
                    self?.fetchUsers()
                    self?.fetchMessages()
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        
        private func fetchChannel(uid: String) {
            dataService.getChannel(uid: uid) { [weak self] result in
                switch result {
                case .success(let channel):
                    self?.channel = channel
                    self?.fetchUsers()
                    self?.fetchMessages()
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        
        func fetchMessages() {
            guard let channel = channel else { return }
                      
            dataService.listenChannel(channel) { [weak self] message in
                self?.messages.append(message)
                self?.recentMessage = message
            }
        }
        
        private func fetchUsers()
        {
            guard let channel = channel else { return }
            
            for uid in channel.userIds {
                dataService.getUser(uid) { [weak self] result in
                    switch result {
                    case .success(let user):
                        self?.users.append(user)
                        
                        // Name the channel based on other user
                        if user.uid != self?.sender.uid {
                            self?.receiver = user
                            self?.chatRooomTitle = user.displayName ?? user.email
                        }

                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}
