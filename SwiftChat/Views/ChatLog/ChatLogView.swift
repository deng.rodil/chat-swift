//
//  ChatLogView.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/17/22.
//

import SwiftUI

struct ChatLogView: View {
    
    @StateObject private var vm: ChatLogView.ViewModel
    
    init(channelId: String, sender: User) {
        _vm = .init(wrappedValue: .init(channelId: channelId, sender: sender))
    }
    
    // Used for direct channels
    init(receiver: User, sender: User) {
        _vm = .init(wrappedValue: .init(receiver: receiver, sender: sender))
    }

    var body: some View {
        
        VStack {
            Spacer()
            messageList
            chatBar
        }
        .navigationTitle(vm.chatRooomTitle)
        .navigationBarTitleDisplayMode(.inline)
        .onAppear(perform: vm.fetchData)
    }
    
    private var messageList: some View {
        // Messages
        ScrollView {
            
            ScrollViewReader { proxy in
                ForEach(vm.messages) { message in
                    messageView(message: message)
                        .id(message.uid)
                }
                .padding(.horizontal)
                .padding(.top, 8)
                .onChange(of: vm.recentMessageSent) { _ in
                    withAnimation {
                        proxy.scrollTo(vm.recentMessageSent?.uid, anchor: .bottom)
                    }
                }
                
                HStack {
                    Spacer()
                }
            }
        }
        .background(Color(.init(white: 0.95, alpha: 1)))
    }
    
    private func messageView(message: Message) -> some View {
        VStack {
            if message.senderUid == vm.sender.uid {
                HStack {
                    Spacer()
                    HStack {
                        Text(message.text)
                            .foregroundColor(.white)
                    }
                    .padding()
                    .background(Color.blue)
                    .cornerRadius(8)
                }
            }
            else {
                HStack {
                    HStack {
                        Text(message.text)
                            .foregroundColor(.black)
                    }
                    .padding()
                    .background(Color.white)
                    .cornerRadius(8)
                    Spacer()
                }
            }
        }
    }
    
    private var chatBar: some View {
        HStack(spacing: 16) {
            Image(systemName: "photo.on.rectangle")
                .font(.system(size: 24))
                .foregroundColor(Color(.darkGray))
            TextField("Description", text: $vm.chatText)
                .multilineTextAlignment(.leading)
//                TextEditor(text: $chatText)
//                    .frame(maxHeight: 60, alignment: .leading)
//                    .multilineTextAlignment(.leading)
//                    .foregroundColor(Color(.label))
//                    .border(Color.yellow, width: 3)
            Button {
                vm.sendText()
            } label: {
                Text("Send")
                    .foregroundColor(.white)
            }
            .padding(.horizontal)
            .padding(.vertical, 8)
            .background(Color.blue)
            .cornerRadius(4)

        }
        .padding(.horizontal)
        .padding(.vertical, 8)
    }
}

struct ChatLogView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ChatLogView(channelId: Channel.test.uid, sender: User.fie)
        }
    }
}
