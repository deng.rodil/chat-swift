//
//  ProfileViewModel.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/25/22.
//

import Foundation
import SwiftUI

extension ProfileView {
    class ViewModel: ObservableObject {
        @Dependency var dataService: DataService
        
        @Published var displayName = ""
        @Published var picture: UIImage?
        
        private(set) var user: User
        
        init(user: User) {
            self.user = user
            self.displayName = user.email
        }
        
        func fetchData() {
            dataService.getUser(user.uid) { [weak self] result in
                switch result {
                case .success(let user):
                    self?.user = user
                    self?.displayName = user.displayName ?? user.email
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        
        func uploadProfilePicture(image: UIImage) {
            print("Uploading profile picture...")
            guard let data = image.jpegData(compressionQuality: 0.3) else { return }
            
            dataService.uploadProfilePicture(user: user, imageData: data) { [weak self] result in
                switch result {
                case .success(let url):
                    self?.user.profilePictureUrl = url
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
