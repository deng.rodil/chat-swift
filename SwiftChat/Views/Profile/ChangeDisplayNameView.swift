//
//  ChangeDisplayNameView.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/25/22.
//

import SwiftUI

struct ChangeDisplayNameView: View {
    
    @StateObject private var vm: ChangeDisplayNameView.ViewModel
    
    @Environment(\.dismiss) var dismiss
    @State var errorMessage = ""
    
    init(user: User) {
        _vm = .init(wrappedValue: .init(user: user))
    }

    var body: some View {
        VStack() {
            HStack {
                Text("Display Name")
                    .font(.system(size: 16))
                    .foregroundColor(Color(.label))
                Spacer()
            }
            
            HStack {
                TextField(vm.user.displayName ?? "Set display name", text: $vm.text)
                Spacer()
            }
            
            Button {
                onUpdate()
            } label: {
                HStack {
                    Spacer()
                    Text("Update")
                        .foregroundColor(.white)
                        .padding(.vertical, 10)
                        .font(.system(size: 14, weight: .semibold))
                    Spacer()
                }.background(Color.blue)
            }
            .padding(.top, 80)
            
            Text(errorMessage)
                .foregroundColor(.red)
            
            Spacer()
        }
        .padding()
    }
    
    private func onUpdate() {
        errorMessage = ""
        vm.update { result in
            switch result {
            case .success(_):
                dismiss()
            case .failure(let error):
                self.errorMessage = error.localizedDescription
            }
        }
    }
}

struct ChangeDisplayNameView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ChangeDisplayNameView(user: .fie)
        }
    }
}
