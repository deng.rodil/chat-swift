//
//  ProfileView.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/25/22.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProfileView: View {
    
    @StateObject private var vm: ProfileView.ViewModel
    @State var shouldShowImagePicker = false
    @State var picture: UIImage?

    init(user: User) {
        _vm = .init(wrappedValue: .init(user: user))
    }
    
    var body: some View {
        ScrollView {
            VStack(spacing: 16) {
                
                header
                    .padding(.bottom, 24)
                settingsList
            }
            .background(Color(.init(white: 0, alpha: 0.05))
                            .ignoresSafeArea())
        }
        .navigationTitle("Profile")
        .onAppear(perform: vm.fetchData)
    }

    var header: some View {
        VStack() {
            // Account picture
            HStack {
                Button {
                    shouldShowImagePicker.toggle()
                } label: {
                    
                    if let url = vm.user.profilePictureUrl {
                        WebImage(url: url)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 128, height: 128)
                            .cornerRadius(64)
                    } else {
                        Image(systemName: "person.fill")
                            .font(.system(size: 64))
                            .foregroundColor(Color(.label))
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 64)
                                        .stroke(Color.black, lineWidth: 3))
                    }
                }
                .fullScreenCover(isPresented: $shouldShowImagePicker) {
                    if let picture = picture {
                        vm.uploadProfilePicture(image: picture)
                    }
                } content: {
                    ImagePicker(image: $picture)
                }
                
                Spacer()
            }
            
            // Display name
            HStack {
                // Display name
                Text(vm.displayName)
                    .font(.title2)
                    .fontWeight(.semibold)
                Spacer()
            }
        }
        .padding()
        .background(Color(.init(white: 0, alpha: 0.1)))
    }
    
    var settingsList: some View {
        ScrollView {
            
            // Display name
            NavigationLink {
                ChangeDisplayNameView(user: vm.user)
            } label: {
                HStack(spacing: 16) {
                    Text("Display Name")
                        .font(.system(size: 16))
                        .foregroundColor(Color(.label))
                    Spacer()
                    Text(vm.user.displayName ?? "Not yet set")
                        .font(.system(size: 16, weight: .medium))
                        .foregroundColor(Color(.label))
//                    TextField("Fie Claussel", text: $testText)
//                        .textFieldStyle(.automatic)
//                        .font(.system(size: 16, weight: .medium))
//                        .multilineTextAlignment(.trailing)
                }
                .padding(.horizontal)
            }
            Divider()
                .padding(.vertical, 8)
            
            // Email
            NavigationLink {
                Text("Changing of email is not yet implemented")
            } label: {
                HStack(spacing: 16) {
                    Text("Email")
                        .font(.system(size: 16))
                        .foregroundColor(Color(.label))
                    Spacer()
                    Text(vm.user.email)
                        .font(.system(size: 16, weight: .medium))
                        .foregroundColor(Color(.label))
                }
                .padding(.horizontal)
            }
            Divider()
                .padding(.vertical, 8)
            
            // Password
            NavigationLink {
                Text("Changing of password is not yet implemented")
            } label: {
                HStack(spacing: 16) {
                    Text("Change Password")
                        .font(.system(size: 16))
                        .foregroundColor(Color(.label))
                    Spacer()
                }
                .padding(.horizontal)
            }
            Divider()
                .padding(.vertical, 8)
            
        }
        .padding(.bottom, 50)
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ProfileView(user: .fie)
        }
    }
}
