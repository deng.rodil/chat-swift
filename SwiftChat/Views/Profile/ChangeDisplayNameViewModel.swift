//
//  ChangeDisplayNameViewModel.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/25/22.
//

import Foundation

extension ChangeDisplayNameView {
    class ViewModel: ObservableObject {
        @Dependency var dataService: DataService
        
        @Published private(set) var user: User
        @Published var text: String
        
        var isModified: Bool {
            user.displayName != text
        }
        
        init(user: User) {
            self.user = user
            self.text = user.displayName ?? ""
        }
        
        func update(_ completion: @escaping (Result<String,Error>) -> ()) {
            user.displayName = self.text
            dataService.updateProfile(user) { result in
                switch result {
                case .success(_):
                    completion(.success(self.text))
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(.failure(error))
                }
            }
        }
    }
}
