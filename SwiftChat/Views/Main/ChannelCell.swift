//
//  MainMessageCell.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/5/22.
//

import SwiftUI
import SDWebImageSwiftUI

struct ChannelCell: View {
    
    @StateObject private var vm: ChannelCell.ViewModel
    
    init(channel: Channel) {
        _vm = .init(wrappedValue: .init(channel: channel))
    }

    var body: some View {
        VStack {
            HStack(spacing: 16) {

                if let picUrl = vm.channelPictureUrl {
                    WebImage(url: picUrl)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 50, height: 50)
                        .clipped()
                        .cornerRadius(50)
                        .overlay(RoundedRectangle(cornerRadius: 50)
                                    .stroke(Color(.label), lineWidth: 2))
                }
                else {
                    Image(systemName: "person.fill")
                        .font(.system(size: 36))
                        .frame(width: 50, height: 50)
                        .overlay(RoundedRectangle(cornerRadius: 50)
                                    .stroke(Color(.label), lineWidth: 2))
                }

                VStack(alignment: .leading) {
                    Text(vm.channelName)
                        .font(.system(size: 16, weight: .semibold))
                        .foregroundColor(Color(.label))
                    Text(vm.recentMessage)
                        .font(.system(size: 14))
                        .foregroundColor(Color(.lightGray))
                }
                Spacer()
                
                Text(vm.sinceRecentMessage)
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(Color(.label))
            }
            .onAppear(perform: vm.fetchData)
        }
    }
}

struct MainMessageCell_Previews: PreviewProvider {
    static var previews: some View {
        ChannelCell(channel: Channel.test)
    }
}
