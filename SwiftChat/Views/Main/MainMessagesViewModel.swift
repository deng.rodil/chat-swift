//
//  MainMessagesPresenter.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/10/22.
//

import Foundation
import SwiftUI

extension MainMessagesView {
    class ViewModel: ObservableObject {
        @Dependency private var dataService: DataService

        @Published var user: User
        @Published private(set) var currentUserPictureUrl: URL?
        @Published var channels: [Channel] = []
        
        var currentUserName: String { dataService.currentUser?.email ?? "N/A" }

        init(user: User) {
            self.user = user
        }
        
        func fetchData() {
            if let authUser = dataService.currentUser {
                user = authUser
                currentUserPictureUrl = user.profilePictureUrl
            }
            fetchChannels()
        }
        
        func logout() {
            do {
                try dataService.signOut()
            } catch {
                print(error.localizedDescription)
            }
        }

        private func fetchChannels() {
            dataService.getInvolvedChannels(user: user) { [weak self] result in
                switch result {
                case .success(let channels):
                    self?.channels = channels
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
