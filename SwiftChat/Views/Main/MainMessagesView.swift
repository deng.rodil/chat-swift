//
//  MainMessageView.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/5/22.
//

import SwiftUI
import SDWebImageSwiftUI

struct MainMessagesView: View {
    
    @StateObject private var vm: MainMessagesView.ViewModel

    @State private var isLogoutShown = false
    @State private var shouldLogin = true
    @State private var shouldShowImagePicker = false
    @State private var selectedPicture: UIImage?
    @State private var shouldShowNewMessage = false

    init(user: User) {
        _vm = .init(wrappedValue: .init(user: user))
    }

    var body: some View {
        VStack {
            
            // Login
            NavigationLink("Login", isActive: $shouldLogin) {
                LoginView { user in
                    shouldLogin = false
                    self.vm.user = user
                }
            }
            .hidden()

            // Navigation bar
            navigationBar
            
            // Message list
            channelList
        }
        .overlay(newMessageButton, alignment: .bottom)
        .navigationBarHidden(true)
        .onAppear(perform: vm.fetchData)
    }
    
    var navigationBar: some View {
        HStack(spacing: 16) {
            
            NavigationLink {
                ProfileView(user: vm.user)
            } label: {
                if vm.currentUserPictureUrl != nil {
                    WebImage(url: vm.currentUserPictureUrl)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 50, height: 50)
                        .clipped()
                        .cornerRadius(50)
                        .overlay(RoundedRectangle(cornerRadius: 44)
                                    .stroke(Color(.label), lineWidth: 1))
                }
                else {
                    Image(systemName: "person.fill")
                        .font(.system(size: 36, weight: .heavy))
                        .frame(width: 50, height: 50)
                        .overlay(RoundedRectangle(cornerRadius: 44)
                                    .stroke(Color(.label), lineWidth: 1))
                }
            }
            
            VStack(alignment: .leading, spacing: 4) {
                Text(vm.user.displayName ?? vm.user.email)
                    .font(.system(size: 24, weight: .bold))
                
                HStack {
                    Circle()
                        .foregroundColor(.green)
                        .frame(width: 14, height: 14)
                    Text("online")
                        .font(.system(size: 12))
                        .foregroundColor(Color(.lightGray))
                }
            }
            
            Spacer()
            
            Button {
                isLogoutShown.toggle()
            } label: {
                Image(systemName: "gear")
                    .font(.system(size: 24, weight: .bold))
                    .foregroundColor(Color(.label))
            }
        }
        .padding()
        .actionSheet(isPresented: $isLogoutShown) {
            .init(title: Text("Settings"), message: Text("What do you want to do?"), buttons: [.destructive(Text("Sign Out"), action: {
                shouldLogin = true
            }),
                                                                                               .cancel()])
        }
    }
    
    var channelList: some View {
        ScrollView {
            ForEach(vm.channels) { channel in
                NavigationLink {
                    ChatLogView(channelId: channel.uid, sender: vm.user)
                } label: {
                    ChannelCell(channel: channel)
                }
                Divider()
                    .padding(.vertical, 8)
            }.padding(.horizontal)
            
        }.padding(.bottom, 50)
    }
    
    var newMessageButton: some View {
        NavigationLink {
            NewMessageUserList(sender: vm.user)
        } label: {
            HStack {
                Spacer()
                Text("+ New Message")
                Spacer()
            }
            .foregroundColor(.white)
            .padding(.vertical)
            .background(Color.blue)
            .cornerRadius(32)
            .padding(.horizontal)
            .shadow(radius: 14)
        }
    }
}

struct MainMessageView_Previews: PreviewProvider {    
    static var previews: some View {
        NavigationView {
            MainMessagesView(user: User.fie)
        }
    }
}
