//
//  MainMessageCellViewModel.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/18/22.
//

import Foundation

extension ChannelCell {
    class ViewModel: ObservableObject {
        @Dependency private var dataService: DataService
        
        private(set) var channel: Channel
        private(set) var authUser: User?
        private var users: [User] = []
        
        @Published var recentMessage: String = ""
        @Published var channelPictureUrl: URL?
        @Published var channelName: String = ""
        @Published var sinceRecentMessage: String = ""

        init(channel: Channel) {
            self.channel = channel
        }
        
        func fetchData()
        {
            authUser = dataService.currentUser
            listenRecentMessage()
            fetchUsers()
            
            dataService.getChannel(uid: channel.uid) { [weak self] result in
                switch result {
                case .success(let channel):
                    self?.channel = channel
                    self?.fetchUsers()
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        
        private func listenRecentMessage() {
            dataService.listenChannel(self.channel) { [weak self] message in
                self?.recentMessage = message.text
                
                let date = Date(timeIntervalSince1970: TimeInterval(message.timestamp.seconds))
                let diff = Date.now.timeIntervalSince(date)
                let minutes = Int(diff / 60)
                let hours = Int(minutes / 60)
                let days = Int(hours / 24)
                if days > 0 {
                    self?.sinceRecentMessage = "\(days)d"
                }
                else if hours > 0 {
                    self?.sinceRecentMessage = "\(hours)h"
                }
                else if minutes > 0 {
                    self?.sinceRecentMessage = "\(minutes)m"
                }
                else {
                    self?.sinceRecentMessage = "Now"
                }
            }
        }
        
        private func fetchUsers()
        {
            for uid in channel.userIds {
                dataService.getUser(uid) { [weak self] result in
                    switch result {
                    case .success(let user):
                        self?.users.append(user)

                        // Name the channel based on other user
                        if user.uid != self?.authUser?.uid {
                            self?.channelName = user.displayName ?? user.email
                            self?.channelPictureUrl = user.profilePictureUrl
                        }

                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}
