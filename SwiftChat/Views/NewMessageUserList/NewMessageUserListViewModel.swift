//
//  NewMessageUserListPresenter.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/12/22.
//

import Foundation

extension NewMessageUserList {
    class ViewModel: ObservableObject {
        
        @Dependency private var dataService: DataService
        
        private(set) var sender: User
        @Published var users: [User] = []

        init(sender: User) {
            self.sender = sender
        }
        
        func fetchData() {

            dataService.fetchAllUsers { [weak self] result in
                switch result {
                case .success(let users):
                    // Do not include current user
                    if let currentUser = self?.dataService.currentUser {
                        self?.users = users.filter { user in
                            user.uid != currentUser.uid
                        }
                    }
                    else {
                        self?.users = users
                    }
                    
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
