//
//  NewMessageUserList.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/12/22.
//

import SwiftUI
import SDWebImageSwiftUI

struct NewMessageUserList: View {
    
    @StateObject private var vm: NewMessageUserList.ViewModel
    
    init(sender: User) {
        _vm = .init(wrappedValue: .init(sender: sender))
    }
    
    var body: some View {
        ScrollView {
            ForEach(vm.users) { user in
                NavigationLink {
                    ChatLogView(receiver: user, sender: vm.sender)
                } label: {
                    HStack(spacing: 16) {
                        WebImage(url: user.profilePictureUrl)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 50, height: 50)
                            .clipped()
                            .cornerRadius(50)
                            .overlay(RoundedRectangle(cornerRadius: 50)
                                        .stroke(Color(.label), lineWidth: 2))
                        Text(user.displayName ?? user.email)
                        Spacer()
                    }
                    .padding(.horizontal)
                }
                Divider()
                    .padding(.vertical, 8)
            }
        }
        .navigationTitle("New Message")
        .onAppear(perform: vm.fetchData)
    }
}

struct NewMessageUserList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            NewMessageUserList(sender: User.fie)
        }
    }
}
