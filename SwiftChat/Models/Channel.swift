//
//  Channel.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/19/22.
//

import Foundation
import Firebase

struct Channel: Identifiable {
    
    var id: String { uid }
    
    let uid: String
    let userIds: [String]
    let directMessage: Bool
    
    var messages: [Message]?
    var recentMessage: Message? {
        guard let messages = messages else { return nil }
        return messages.sorted { m1, m2 in
            m1.timestamp.nanoseconds > m2.timestamp.nanoseconds
        }.first
    }
    
    var users: [User]?
    
    var info: ChannelCompleteInfo?

    func completeWith(info: ChannelCompleteInfo) -> Channel {
        var new = self
        new.info = info
        return new
    }
}

struct ChannelCompleteInfo {
    var messages: [Message]
    var users: [User]
    var lastMessage: Message?
    {
        messages.sorted { m1, m2 in
            m1.timestamp.nanoseconds > m2.timestamp.nanoseconds
        }.first
    }
}
