//
//  TestModels.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/21/22.
//

import Foundation

extension User {
    static let fie = User(uid: "xkwtzCGBGaQEWxCYCgS0X4tvq2S2", displayName: "Fie", email: "fie@gmail.com", profilePictureUrl: URL(string: "https://cdn.myanimelist.net/images/characters/8/355778.jpg"))
    static let alisa = User(uid: "1M4JNAF5eXe0MzGY0liGByDowTd2", displayName: "Alisa", email: "alisa@gmail.com", profilePictureUrl: URL(string: "https://cdn.myanimelist.net/images/characters/8/355778.jpg"))
}

extension Channel {
    static let test = Channel(uid: "xk4wKHeLjCpNyupQNUJb", userIds: [User.fie.uid, User.alisa.uid], directMessage: true, info: nil)
}
