//
//  User.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/4/22.
//

import Foundation

struct User: Identifiable {
    var id: String { uid }
    
    let uid: String
    var displayName: String?
    let email: String
    var profilePictureUrl: URL?
}
