//
//  Message.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/19/22.
//

import Foundation
import Firebase

struct Message: Identifiable, Equatable {
    
    var id: String { uid }
    
    let uid: String
    let senderUid: String
    let text: String
    let timestamp: Timestamp
    var info: MessageCompleteInfo?
    
    func completeWith(info: MessageCompleteInfo) -> Message {
        var new = self
        new.info = info
        return new
    }
    
    static func == (lhs: Message, rhs: Message) -> Bool {
        lhs.uid == rhs.uid
    }
}

struct MessageCompleteInfo {
    let sender: User
}
