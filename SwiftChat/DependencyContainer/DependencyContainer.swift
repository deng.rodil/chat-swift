//
//  Services.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/12/22.
//

import Foundation

final class DependencyContainer: ObservableObject {
    static let shared: DependencyContainer = DependencyContainer()

    var dependencies: [String: Any] = [:]
    
    func register<T>(_ dependency: T) {
        let key = String(describing: T.self)
        dependencies[key] = dependency
    }
    
    func get<T>() -> T {
        let key = String(describing: T.self)
        let dependency = dependencies[key]
        precondition(dependency != nil, "No dependency found for \(key)")
        
        return dependency as! T
    }
    
    static func register<T>(_ dependency: T) {
        shared.register(dependency)
    }
    
    static func get<T>() -> T {
        shared.get()
    }
}

@propertyWrapper
struct Dependency<T> {
    var wrappedValue: T
    
    init() {
        self.wrappedValue = DependencyContainer.get()
    }
}
