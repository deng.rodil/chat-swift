//
//  SwiftChatApp.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/3/22.
//

import SwiftUI
import Firebase

@main
struct SwiftChatApp: App {
    
    init() {
        configureDependencies()
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            NavigationView() {
                MainMessagesView(user: User.fie)
            }
            .navigationViewStyle(.stack)
        }
    }
    
    func configureDependencies() {
        let container = DependencyContainer.shared
        
        container.register(FirebaseDataService() as DataService)
    }
}
