//
//  FirebaseDataLayer.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/10/22.
//

import Foundation
import Firebase

class FirebaseDataService: DataService {
    var currentUser: User?
    
    func createAccount(email: String,
                       password: String,
                       completion: @escaping (Result<User, Error>) -> ())
    {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            self.currentUser = User(uid: Auth.auth().currentUser!.uid, email: email)
            completion(.success(self.currentUser!))
            
            // Create profile data
            if let user = self.currentUser {
                let data: [String:Any] = [
                    "email": user.email,
                    "uid": user.uid,
                    "profilePicture": NSNull()
                ]
                Firestore.firestore().collection("users").document(user.uid).setData(data) { error in
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }
                }
            }
        }
    }

    func signIn(email: String,
                password: String,
                completion: @escaping (Result<User, Error>) -> ())
    {
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] result, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            // Get profile data
            let user = User(uid: Auth.auth().currentUser!.uid, email: email)
            Firestore.firestore().collection("users").document(user.uid).getDocument { snapshot, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                let data = snapshot!.data()!
                let user = User(fireStoreData: data)
                self?.currentUser = user
                completion(.success(user))
            }
        }
    }
    
    func signOut() throws {
        try Auth.auth().signOut()
    }
    
    func getUser(_ uid: String,
                 completion: @escaping (Result<User, Error>) -> ()) {
        Firestore.firestore().collection("users").document(uid).getDocument { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            completion(.success(.init(fireStoreData: snapshot!.data()!)))
        }
    }
    
    func retrieveProfilePicture(uid: String,
                                completion: @escaping (Result<Data, Error>) -> ()) {
        let ref = Storage.storage().reference(withPath: "profilepics/\(uid).jpg")
        ref.getData(maxSize: 1024 * 1024) { data, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            completion(.success(data!))
        }
    }
    
    func uploadProfilePicture(user: User,
                              imageData: Data,
                              completion: @escaping (Result<URL, Error>) -> ())
    {
        // Upload image
        let ref = Storage.storage().reference(withPath: "profilepics/\(user.uid).jpg")
        ref.putData(imageData, metadata: nil) { metaData, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            // Update image url
            ref.downloadURL { url, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }

                // Update profile
                Firestore.firestore().collection("users").document(user.uid).updateData(["profilePicture": url!.absoluteString]) { error in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    
                    completion(.success(url!))
                }
            }
        }
    }
    
    func updateProfile(_ user: User,
                       completion: @escaping (Result<Void, Error>) -> ())
    {
        let data: [String:Any] = [
            "email": user.email,
            "uid": user.uid,
            "displayName": user.displayName ?? NSNull(),
            "profilePicture": user.profilePictureUrl?.absoluteString ?? NSNull()
        ]
        
        Firestore.firestore().collection("users").document(user.uid).setData(data, merge: true) { error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            completion(.success(Void()))
        }
    }
    
    func fetchAllUsers(completion: @escaping (Result<[User], Error>) -> ())
    {
        Firestore.firestore().collection("users").getDocuments { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            var users = [User]()
            snapshot?.documents.forEach({ snapshot in
                let user = User(fireStoreData: snapshot.data())
                users.append(user)
            })
            completion(.success(users))
        }
    }
    
    func sendText(from sender: User,
                  to receiver: User,
                  text: String,
                  completion: @escaping (Result<Message, Error>) -> ())
    {
        // Get direct channel
        getDirectChannel(from: sender, to: receiver) { result in
            switch result {
            case .success(let channel):
                // Send message to the channel
                let document = Firestore.firestore().collection("channels").document(channel.uid).collection("messages").document()
                let data: [String : Any] =
                [
                    "uid": document.documentID,
                    "sender": sender.uid,
                    "text": text,
                    "timestamp": Timestamp()
                ]
                document.setData(data) { error in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    completion(.success(.init(firestoreData: data)))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getDirectChannel(from sender: User, to receiver: User, completion: @escaping (Result<Channel,Error>) -> ())
    {
        let channelsRef = Firestore.firestore().collection("channels")
        let query = channelsRef.whereField("directMessage", isEqualTo: true)
            .whereField("users", arrayContains: sender.uid)
        
        query.getDocuments { snapshot, error in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(error))
                return
            }
            
            // Filter channel that coontains both sender and receiver only
            var channels = [Channel]()
            snapshot?.documents.forEach({ snapshot in
                channels.append(.init(firestoreData: snapshot.data()))
            })
            let channel = channels.first { c in
                c.userIds.contains(sender.uid) && c.userIds.contains(receiver.uid)
            }
            
            // Channel found
            if let channel = channel {
                completion(.success(channel))
                return
            }
            
            // Create channel since it doesn't exist yet
            let channelRef = Firestore.firestore().collection("channels").document()
            
            let data: [String : Any] =
            [
                "uid": channelRef.documentID,
                "users": [sender.uid, receiver.uid],
                "createdBy": sender.uid,
                "directMessage": true,
            ]
            
            channelRef.setData(data) { error in
                if let error = error {
                    print(error.localizedDescription)
                    completion(.failure(error))
                    return
                }
                completion(.success(.init(firestoreData: data)))
            }
        }
    }
    
    func getInvolvedChannels(user: User,
                             completion: @escaping (Result<[Channel], Error>) -> ())
    {
        let channelsRef = Firestore.firestore().collection("channels")
        let query = channelsRef.whereField("users", arrayContains: user.uid)
        
        query.getDocuments { snapshot, error in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(error))
                return
            }
            
            var channels = [Channel]()
            snapshot?.documents.forEach({ snapshot in
                channels.append(.init(firestoreData: snapshot.data()))
            })
            completion(.success(channels))
        }
    }

    func getChannel(uid: String,
                    completion: @escaping (Result<Channel, Error>) -> ())
    {
        Firestore.firestore().collection("channels").document(uid).getDocument { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }

            completion(.success(.init(firestoreData: snapshot!.data()!)))
        }
    }

    func sendText(_ text: String,
                  sender: User,
                  channel: Channel,
                  completion: @escaping (Result<Message, Error>) -> ())
    {
        // Send message to the channel
        let document = Firestore.firestore().collection("channels").document(channel.uid).collection("messages").document()
        let data: [String : Any] =
        [
            "uid": document.documentID,
            "sender": sender.uid,
            "text": text,
            "timestamp": Timestamp()
        ]
        document.setData(data) { error in
            if let error = error {
                completion(.failure(error))
                return
            }
            let message = Message(firestoreData: data).completeWith(info: .init(sender: sender))
            completion(.success(message))
        }
    }
    
    // DOES NOT WORK
    private func fetchUsers(uids: [String], completion: @escaping (Result<[User], Error>) -> ())
    {
        guard !uids.isEmpty else {
            completion(.success([User]()))
            return
        }
        
        // Fetch users
        var users = [User]()
        for i in 0..<uids.count {
            let uid = uids[i]
            Firestore.firestore().collection("users").document(uid).getDocument { snapshot, error in
                if let error = error {
                    print(error.localizedDescription)
                    completion(.failure(error))
                    return
                }
                users.append(.init(fireStoreData: snapshot!.data()!))
                if i == uids.count - 1 {
                    completion(.success(users))
                }
            }
        }
    }
    
    func listenChannel(_ channel: Channel,
                       updated: @escaping (Message) -> ())
    {
        Firestore.firestore().collection("channels").document(channel.uid).collection("messages").order(by: "timestamp").addSnapshotListener { querySnapshot, error in
            if error != nil {
                return
            }
            
            // Capture all messages
//            var messages = [Message]()
//            querySnapshot?.documents.forEach({ snapshot in
//                messages.append(.init(firestoreData: snapshot.data()))
//            })

            querySnapshot?.documentChanges.forEach { change in
                guard change.type == .added else { return }
                updated(.init(firestoreData: change.document.data()))
            }
        }
    }
}

extension User {
    init(fireStoreData data: [String : Any]) {
        self.uid = data["uid"] as! String
        self.email = data["email"] as! String
        self.displayName = data["displayName"] as? String
        
        if let profilePicture = data["profilePicture"] as? String {
            self.profilePictureUrl = URL(string: profilePicture)
        }
    }
}

extension Channel {
    init(firestoreData data: [String : Any]) {
        self.uid = data["uid"] as! String
        self.userIds = data["users"] as! [String]
        self.directMessage = data["directMessage"] as! Bool
    }
}

extension Message {
    init(firestoreData data: [String : Any]) {
        self.uid = data["uid"] as! String
        self.senderUid = data["sender"] as! String
        self.text = data["text"] as! String
        self.timestamp = data["timestamp"] as! Timestamp
    }
}
