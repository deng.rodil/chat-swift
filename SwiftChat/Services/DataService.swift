//
//  DataLayer.swift
//  SwiftChat
//
//  Created by Deng Rodil on 1/10/22.
//

import Foundation
import SwiftUI

protocol DataService {
    var currentUser: User? { get }
    
    func createAccount(email: String,
                       password: String,
                       completion: @escaping (Result<User, Error>) -> ())
    func signIn(email: String,
                password: String,
                completion: @escaping (Result<User, Error>) -> ())
    func signOut() throws
    
    func getUser(_ uid: String,
                 completion: @escaping (Result<User, Error>) -> ())
    func fetchAllUsers(completion: @escaping (Result<[User], Error>) -> ())
    
    func retrieveProfilePicture(uid: String,
                                completion: @escaping (Result<Data, Error>) -> ())
    func uploadProfilePicture(user: User,
                              imageData: Data,
                              completion: @escaping (Result<URL, Error>) -> ())
    func updateProfile(_ user: User,
                       completion: @escaping (Result<Void, Error>) -> ())
    
    func sendText(from sender: User,
                  to receiver: User,
                  text: String,
                  completion: @escaping (Result<Message, Error>) -> ())
    
    func getInvolvedChannels(user: User,
                             completion: @escaping (Result<[Channel], Error>) -> ())
    func getDirectChannel(from sender: User,
                          to receiver: User,
                          completion: @escaping (Result<Channel, Error>) -> ())
    func getChannel(uid: String,
                    completion: @escaping (Result<Channel, Error>) -> ())

    func sendText(_ text: String,
                  sender: User,
                  channel: Channel,
                  completion: @escaping (Result<Message, Error>) -> ())
    func listenChannel(_ channel: Channel,
                       updated: @escaping (Message) -> ())
}
