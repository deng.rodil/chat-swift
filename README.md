# Chat App
A simple real-time chat application aimed to familiarize myself with native iOS app development.

This is based on [Let's Build That App](https://www.youtube.com/c/LetsBuildThatApp) so I have a rough idea on the basic features of the platform and technology used to build the app. However, implementation is based on my own interpretation of the software architecture.

## Preview
<img src="https://i.imgur.com/S6AEQ7l.png" width="250">
<img src="https://i.imgur.com/rdX5k4r.png" width="250">
<img src="https://i.imgur.com/E6zhhO6.png" width="250">
<img src="https://i.imgur.com/dGsOhSn.png" width="250">
<img src="https://i.imgur.com/COwQLgI.png" width="250">
<img src="https://i.imgur.com/WrPjRmk.png" width="250">

## Features

 - Messaging
	 - Sending and receiving real-time text message
 - User Account
	 - Creating an account
	 - Authentication
	 - Editing user's profile
	 - Uploading profile picture
	 - Signing out
 - Channels
	 - 1:1 channels (direct message)
	 - Displaying list of channels where the user is involved in
	 - Displaying recent messages in a channel

## Features to be implemented

 - Group channels
 - Attachments in message
 - Message unread status
 - Authenticate with other providers (eg. Google)
 - Push notification

## Technical Details

 - Technology
	 - **Swift 5.1** (Language)
	 - **Swift UI** (UI Framework)
	 - **Firebase** (online data storage and authentication)
 - Software Architecture
	 - **MVVM** (UI software architecture)
	 - Practices **SOLID** principles
	 - **Dependency injection** using my own **dependency container**


